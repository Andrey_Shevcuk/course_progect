<?php

namespace models;
/**
 * модель користувачів
 */
use core\Model;
use core\Utils;

class Users extends Model
{
    public function Validate($formRom)
    {
        $arrors = [];
        if (empty($formRom['login'])) {
            $arrors[] = 'Поле логін не заповнене';
        }
        $users = $this->GetUsersByLogin($formRom['login']);
        if (!empty($users))
            $arrors[] = 'Такий користувач вже існує';
        if (empty($formRom['password'])) {
            $arrors[] = 'Поле пароль не заповнене';
        }
        if ($formRom['password'] != $formRom['password2']) {
            $arrors[] = 'Паролі не співпадають';
        }
        if(count(str_split($arrors['password']))>=8){
            $arrors[] = 'Дожина пароля має бути 8 і більше симмволів';
        }
        if (empty($formRom['name'])) {
            $arrors[] = 'Поле імя не заповнене';
        }
        if (count($arrors) > 0) {
            return $arrors;
        } else {
            true;
        }

    }

    public function AddUsers($userRow)
    {
        $valresult = $this->Validate($userRow);
        if (is_array($valresult)) {
            return $valresult;
        }
        $fields = ['login', 'password', 'name'];
        $userRowf = Utils::ArrayFilter($userRow, $fields);
        $userRowf['password'] = md5($userRowf['password']);
        $rows = \core\Core::getInstance()->getDB()->insert('users', $userRowf);
        return true;
    }

    public function GetUsersByLogin($login)
    {
        $rows = \core\Core::getInstance()->getDB()->select('users', '*', ['login' => $login]);
        if (count($rows) > 0) {
            return $rows[0];
        } else {
            return null;
        }

    }
public function isUserAuth(){
        return isset($_SESSION['user']);

}
public function getCurrentUser(){
        if($this->isUserAuth()){
            return $_SESSION['user'];
        }
        else{
            return null;
        }
}
    public function AuthUsers($login, $password)
    {
        $password = md5($password);
        $users = \core\Core::getInstance()->getDB()->select('users','*', ['login' => $login, 'password' => $password]);
         if(count($users)>0){
             $user=$users[0];
             return $user;
         }
         else{
             return false;
         }
    }
}