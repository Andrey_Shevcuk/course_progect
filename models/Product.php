<?php
/**
 * модель продуктів
*/
namespace models;

use core\Utils;

class Product extends \core\Model
{
    public function ChagePhoto($id, $file)
    {
        $folde = 'files/product/';
        $product = $this->GetProductById($id);
        if (is_file($folde . $product['photo']) && is_file($folde . $file)) {
            unlink($folde, $product['photo']);
        }
        $product['photo'] = $file;
        $this->UpdateProduct($product, $id);
    }

    public function AddProduct($row)
    {
        $usermodel = new \models\Users();
        $user = $usermodel->getCurrentUser();
        if ($user == null) {
            $result = [
                'error' => true,
                'massages' => ['Користувач не аунтифікований']
            ];
            return $result;
        }
        $valresult = $this->Validate($row);
        if (is_array($valresult)) {
            $result = [
                'error' => true,
                'massages' => $valresult
            ];
            return $result;
        }
        $fields = ['name', 'cost', 'discrip', 'country', 'view'];
        $ProductRowf = Utils::ArrayFilter($row, $fields);
        $ProductRowf['user_id'] = $user['id'];
        $ProductRowf['photo'] = 'sssssssss.jpg';
        $id = \core\Core::getInstance()->getDB()->insert('product', $ProductRowf);
        return [
            'error' => false,
            'id' => $id
        ];

    }

    public function GetCountProduct()
    {
        return \core\Core::getInstance()->getDB()->select('product', '*', null,null,null,'cost');
    }

    public function GetProductById($id)
    {

        $row = \core\Core::getInstance()->getDB()->select('product', '*', ['id' => $id]);

        if (!empty($row)) {
            return $row[0];
        } else {
            return null;
        }
    }

    public function GetProductByCategory($category)
    {
echo 'sssssssssss';
        $row = \core\Core::getInstance()->getDB()->select('product', '*', ['view' => $category],  null, null, 'cost');

        if (!empty($row)) {
            return $row;
        } else {
            return null;
        }
    }

    public function GetKey($category)
    {
        $cartGoods = [];
        $i = 0;
        foreach ($_COOKIE as $key => $value) {
            if ($key == 'PHPSESSID' || $key == 'XDEBUG_SESSION'||$key=='sum'||$key=='1')
            {

            }
            else{
                $cartGoods[$i] = $key;
                $i++;
            }

        }

        return $cartGoods;
    }

    public
    function UpdateProduct($product, $id)
    {
        $usermodel = new \models\Users();
        $user = $usermodel->getCurrentUser();
        if ($user == null) {
            return false;
        }
        $valresult = $this->Validate($product);
        if (is_array($valresult)) {
            return $valresult;
        }
        $fields = ['name', 'cost', 'discrip', 'country', 'view', 'photo'];
        $ProductRowf = Utils::ArrayFilter($product, $fields);
        \core\Core::getInstance()->getDB()->updata('product', $ProductRowf, ['id' => $id]);
        return true;


    }

    public
    function DeleteProduct($id)
    {

        $product = $this->GetProductById($id);
        $usermodel = new \models\Users();
        $user = $usermodel->getCurrentUser();
        if($user['acses']==1){
            \core\Core::getInstance()->getDB()->delete('product', ['id' => $id]);
            return true;
        }
        elseif (empty($product) || empty($user) || $user == null || $user['id'] != $product['user_id']||$user['acses']!=1) {
            return false;
        }
        else {
            \core\Core::getInstance()->getDB()->delete('product', ['id' => $id]);
            return true;
        }
    }

    public
    function Validate($formRom)
    {

        $arrors = [];
        if (empty($formRom['name'])) {
            $arrors[] = 'Поле "Назва" не заповнене';
        }
        if (empty($formRom['discrip'])) {
            $arrors[] = 'Поле "Опис товару" не заповнене';
        }
        if (empty($formRom['country'])) {
            $arrors[] = 'Поле "Країна виробник" не заповнене';
        }
        if (empty($formRom['cost'])) {
            $arrors[] = 'Поле "Ціна" не заповнене';
        }
        if (empty($formRom['view'])) {
            $arrors[] = 'Поле "Тип" не заповнене';
        }
        if (count($arrors) > 0) {
            return $arrors;
        } else {
            return true;
        }

    }

}