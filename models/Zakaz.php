<?php

namespace models;
/**
 * модель замовлень
 */
use core\Model;
use core\Utils;

class Zakaz extends Model
{
    public function AddProduct()
    {
        $model = new \models\Product();
        $key = $model->GetKey($_COOKIE);
        $row['cost'] = $_COOKIE['sum'];
        $model = new \models\Users();
        $user = $model->getCurrentUser();
        $row['id_user'] = $user['id'];
        \core\Core::getInstance()->getDB()->insert('zazaz_cost', $row);
        $zakaz = \core\Core::getInstance()->getDB()->select('zazaz_cost', '*', null);
        $id_zakaz = $zakaz[count($zakaz) - 1]['id_zakaz'];
        $this->AddProduct2($id_zakaz, $key);
        return $key;
    }
  public function getCost($id_zakaz){

     $row= \core\Core::getInstance()->getDB()->select('zazaz_cost', 'cost', ['id_zakaz' => $id_zakaz]);
    return $row;
  }
    public function getZakaz()
    {
        $modelUser = new \models\Users();
        $modelProduct = new \models\Product();
        $user = $modelUser->getCurrentUser();
        $zakaz1 = \core\Core::getInstance()->getDB()->select('zazaz_cost', '*', ['id_user' => $user['id']]);
        $zakaz2 = [];
        $zakaz3 = [];
        $zakaz4 = [];
        $rezult = [];
        $id_zakaz2 = [];
        $fields = ['name', 'cost', 'photo', 'country', 'view'];
        foreach ($zakaz1 as $product) {
            $id_zakaz [] = $product['id_zakaz'];
        }
        if($id_zakaz!=null) {
            for ($i = 0; $i < count($id_zakaz); $i++) {
                $zakaz2 [] = \core\Core::getInstance()->getDB()->select('zakaz1', '*', ['id_zakaz' => $zakaz1[$i]['id_zakaz']]);
            }


            for ($i = 0; $i < count($zakaz2); $i++) {
                for ($j = 0; $j < count($zakaz2[$i]); $j++) {
                    $zakaz4[$i][$j] = $modelProduct->GetProductById($zakaz2[$i][$j]['id_product']);
                }
            }
            for ($i = 0; $i < count($zakaz4); $i++) {
                for ($j = 0; $j < count($zakaz4[$i]); $j++) {
                    $rezult[$i][] = Utils::ArrayFilter($zakaz4[$i][$j], $fields);
                    $rezult[$i][$j]['id_zakaz'] = $zakaz2[$i][$j]['id_zakaz'];
                    $rezult[$i][$j]['count'] = $zakaz2[$i][$j]['count'];
                }
            }
            return $rezult;
        }
        else{
            return false;
        }


    }

    public function AddProduct2($id_zakaz, $key)
    {
        for ($i = 0; $i < count($key); $i++) {
            $row['id_product'] = $key[$i];
            $row['id_zakaz'] = $id_zakaz;
            $row['count'] = $_COOKIE[$key[$i]];
            var_dump($row);
            \core\Core::getInstance()->getDB()->insert('zakaz1', $row);
            unset($row);
        }

    }
}