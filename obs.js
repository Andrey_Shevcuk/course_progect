
document.onclick=function (event) {
    let button = event.target;
    if (button.classList.contains('btn-buy')) {
        addProd(button.id);

    }
    if (button.classList.contains('btn-add')) {
        addProd(button.id);

    }
    if (button.classList.contains('btn-div')) {
        deleteProd(button.id);

    }
    if (button.classList.contains('btn-del')) {
        deleteCookie(button.id);
    }
}
function addProd(id) {
    console.log("sss");
    let amount = getCookie(id);
    if (amount !== null) {
        let newAmount = parseInt(amount) + 1;
        document.cookie = `${id}=${newAmount}; path=/;`;
    }
    else
        document.cookie = `${id}=1; path=/;`;
}
function getCookie(id) {
    let match = document.cookie.match(new RegExp('(^| )' + id + '=([^;]+)'));
    if (match) return match[2];
    return null;
}
function deleteCookie(id) {
    document.cookie = id + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
function deleteProd(id) {
    let amount = getCookie(id);
    let newAmount = parseInt(amount) - 1;
    console.log(newAmount);
    if (newAmount > 0) {
        document.cookie = `${id}=${newAmount}; path=/;`;
    } else
        document.cookie = `${id}=;expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
}

