
<?php
$count2 = 0;
$productmodel = new models\Product();
$userModel = new models\Users();
?>
<div style="margin: 10px">
    <a href="/product?category=Петарди" class="btn btn-primary">Петарда</a>
    <a href="/product?category=Навчальні гранати" class="btn btn-primary">Навчальні гранати</a>
    <a href="/product?category=Бенгальські свічки" class="btn btn-primary">Бенгальські свічки</a>
    <a href="/product?category=Піротехнічні фонтани" class="btn btn-primary">Піротехнічні фонтани</a>
    <a href="/product?category=Піротехнічні ракети" class="btn btn-primary">Піротехнічні ракети</a>
    <a href="/product?category=Феєрверки" class="btn btn-primary">Феєрверки</a>
    <a href="/product" class="btn btn-primary">Показати всі</a>
</div>
<?php if (!isset($_GET['category'])): ?>
    <?php ;
    $count3 = $count;
    ?>
<?php else: ?>
    <?php $count3 = $productmodel->GetProductByCategory($_GET['category']); ?>
<?php endif; ?>
<?php if (!empty($count3)): ?>
    <div class="container">
        <?php foreach ($count3 as $product): ?>
            <?php if ($count2 == 0) : ?>
                <div class="row justify-content-md-left">
            <?php endif; ?>
            <div class="hover-effect col-md-auto" style="margin: 20px">
                <div class="card" style="width: 400px; ">
                    <img style="height: 300px;width: 400px" src="/files/product/<?= $product['photo'] ?>"
                         class="card-img-top" alt="">
                    <div class="card-body">
                        <h5 class="card-title"><?= $product['name'] ?></h5>
                        <p> Країна виробник.......<?= $product['country'] ?></p>
                        <p>Ціна.........<?= $product['cost'] ?></p>
                        <p>Категорія.........<?= $product['view'] ?></p>
                        <div>
                            <a href="/product/view?id=<?= $product['id'] ?>" class="btn btn-primary">Докладніше</a>
                            <?php if ($userModel->isUserAuth()): ?>
                                <a href="/product/edit?id=<?= $product['id'] ?>" class="btn btn-success">Корегувати</a>

                            <a href="/product/delete?id=<?= $product['id'] ?>" class="btn btn-danger">Видалити</a>
                            <a href="" id="<?= $product['id'] ?>" class="btn btn-success btn-buy">Додати
                                до корзини</a>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
            <?php $count2++; ?>
            <?php if ($count2 + 1 == 3): ?>
                </div>
                <?php $count2 = 0; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php else: ?>
    <h3>В цій категорії немає продуктів</h3>
<?php endif; ?>


