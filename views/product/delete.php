<p>Ви дійсно хочете видалити продукт <b> <?= $model['Name'] ?></b> з сайту?</p>
<p>
    <a href="/product/delete?id=<?= $model['id'] ?>&confirm=yes" class="btn btn-danger">Видалити</a>
    <a href="<?=$_SERVER['HTTP_REFERER']?>" class="btn btn-primary">Відмінити</a>
</p>