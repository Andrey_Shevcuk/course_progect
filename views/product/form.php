
<form method="post" action="" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="file" class="form-label">Фотографія товару</label>
       <input type="file" name="file" accept="image/jpeg,image/png" id="file" class="form-control">
           </div>
    <div class="mb-3">
        <?php if(is_file('files/product/'.$model['photo'])): ?>
        <img src="/files/product/<?=$model['photo']?>">
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <label for="name" class="form-label">Назва товару</label>
        <input type="text" name="name" value="<?= $model['name'] ?>" class="form-control">
    </div>
    <div class="mb-3">
        <label for="discrip" class="form-label">Опис товару</label>
        <textarea name="discrip"  class="form-control editor"><?= $model['discrip'] ?></textarea>
    </div>
    <div class="mb-3">
        <label for="country" class="form-label">Країна виробник</label>
        <input type="text" name="country" value="<?= $model['country'] ?>" class="form-control">
    </div>
    <div class="mb-3">
        <label for="cost" class="form-label">Ціна</label>
        <input type="number"  name="cost" value="<?= $model['cost'] ?>" class="form-control">
    </div>
    <div class="mb-3">
        <label for="view" class="form-label" value>Тип</label>
        <select  class="form-select" id="view" name="view">
            <option value="Петарди"  name="gpu">Петарди</option>
            <option value="Навчальні гранати"  name="cpu">Навчальні гранати</option>
            <option value="Бенгальські свічки"  name="ram">Бенгальські свічки</option>
            <option value="Піротехнічні фонтани"  name="plat">Піротехнічні фонтани</option>
            <option value="Піротехнічні ракети"  name="cool">Піротехнічні ракети</option>
            <option value="Феєрверки"  name="storage">Феєрверки</option>
        </select>
    </div>

    <button type="submit" class="btn btn-primary">Зберегти</button>
</form>
