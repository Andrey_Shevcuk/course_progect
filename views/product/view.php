<div class="container">
        <div class="mb-3">
            <?php if(is_file('files/product/'.$model['photo'])): ?>
                <img src="/files/product/<?=$model['photo']?>">
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="discrip" class="form-label">Опис товару</label>
            <textarea readonly name="discrip"  class="form-control editor"><?= $model['discrip'] ?></textarea>
        </div>
        <div class="mb-3">
            <label for="country" class="form-label">Країна виробник</label>
            <input readonly type="text" name="country" value="<?= $model['country'] ?>" class="form-control">
        </div>
        <div class="mb-3">
            <label for="cost" class="form-label">Ціна</label>
            <input readonly type="text"  name="cost" value="<?= $model['cost'] ?>" class="form-control">
        </div>
        <div class="mb-3">
            <label for="view" class="form-label" value>Тип</label>
            <input readonly type="text"  name="cost" value="<?= $model['view'] ?>" class="form-control">
        </div>
       <a href="/product"  class="btn btn-success btn-add""><-Повернутися</a>
</div>