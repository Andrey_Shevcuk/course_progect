<?php
$userModel = new \models\Users();
$user = $userModel->getCurrentUser();
?>
<!doctype html>
<html>
<head>
    <title><?= $MainTitle ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
<script src="..\obs.js"></script>
<?php if ($userModel->isUserAuth()): ?>
<?php endif; ?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#"></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="/">Головна</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/product">Товари</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/product/add">Додати товар</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/basket">Корзина</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/zakaz">Мої заммовлення</a>
                </li>
            </ul>
            <form class="d-flex">
                <?php if (!$userModel->isUserAuth()): ?>

                    <a href="/users/register" class="btn btn-outline-primary">Реєстрація</a>
                    <a href="/users/login" class="btn btn-primary">Увійти</a>
                <?php else: ?>
                    <span class="nav-link"><?= $user['name'] ?></span>
                    <a href="/users/logout" class="btn btn-primary btn-exit">Вийти</a>
                <?php endif; ?>
            </form>
        </div>
    </div>
</nav>
<div class="container">
    <h1 class="mt-5"><?= $PageTitle ?></h1>
    <?php if (!empty($MassageText)): ?>
        <div class="alert alert-<?= $MassageClass ?>">
            <?= $MassageText ?>
        </div>
    <?php endif; ?>
    <?php ?>
    <?= $PageContent ?>
</div>
</body>
</html>
