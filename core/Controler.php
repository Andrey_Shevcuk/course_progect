<?php

namespace core;
//базовий клас
class Controler
{
    /**
     * Перевірка який використовується метод запиту
    */
    public function isPost()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        return true;
    else
        return false;
    }

    public function isGet()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'GET')
            return true;
        else
            return false;
    }
/**
 * сортування полів $_Post
 */
    public function postFilter($fields)
    {
        return Utils::ArrayFilter($_POST, $fields);
    }
/**
 *Генерує та повертає PageContent для layot/index.php
 * також генерує глобальні та локаліні параметри
*/
    public function render($viewName, $localParams = null, $globalParams = null)
    {
        $tpl = new Template();
        if (is_array($localParams)) {
            $tpl->setParams($localParams);
        }
        if (!is_array($globalParams)) {
            $globalParams = [];
        }
        $modulName = strtolower((new \ReflectionClass($this))->getShortName());
        $globalParams['PageContent'] = $tpl->render("views/{$modulName}/{$viewName}.php");
        return $globalParams;
    }
    /**
     * Генерує та повертає повідомлення (MessageText) для layot/index.php
     */

    public function  rendermassage($type,$massage,$localParams = null, $globalParams = null){
        $tpl = new Template();
        if (is_array($localParams)) {
            $tpl->setParams($localParams);
        }
        $tpl->setParam('MassageText',$massage);
        switch ($type){
            case 'ok':
       $tpl->setParam('MassageClass','success'); break;
            case 'error':
             $tpl->setParam('MassageClass','danger'); break;
            case 'info':
        $tpl->setParam('MassageClass','info'); break;
        }
        if (!is_array($globalParams)) {
            $globalParams = [];
        }
        $modulName = strtolower((new \ReflectionClass($this))->getShortName());
        $globalParams['PageContent'] = $tpl->render("views/layout/massage.php");
        return $globalParams;
    }
}