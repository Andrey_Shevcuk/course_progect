<?php

namespace core;

class DB
{
    public function __construct($server, $login, $password, $database)
    {
        $this->pdo = new \PDO("mysql:host={$server};dbname={$database};charset=UTF8", $login, $password);
    }

    public function select($table, $fields = "*", $where = null, $limit = null, $offset = null, $orderBy = null)
    {

        $fieldsStr = "*";
        if (is_string($fields)) {
            $fieldsStr = $fields;
        }
        if (is_array($fields)) {
            $fieldsStr = implode(', ', $fields);
        }
        $sql = "SELECT {$fieldsStr} FROM {$table} ";
        if (is_array($where) && count($where) > 0) {
            $whereParts = [];
            foreach ($where as $key => $value) {
                $whereParts [] = "{$key}=?";
            }
            $whereStr = implode(' AND ', $whereParts);
            $sql .= ' WHERE ' . $whereStr;

        }

        if (is_string($where)) {
            $sql .= ' WHERE ' . $where;
        }
        if (!empty($orderBy)) {
            $sql .= '  ORDER BY ' .$orderBy ;
        }
        if (!empty($limit)) {
            if (!empty($offset)) {
                $sql .= " LIMIT {$limit} ,{$offset} ";
            } else {
                $sql .= " LIMIT {$limit}  ";
            }
        }
        $sth = $this->pdo->prepare($sql);

        if (is_array($where) && count($where) > 0) {
            $sth->execute(array_values($where));
        } else {
            $sth->execute();
        }
        return $sth->fetchAll();
    }

    public function insert($table, $row)
    {
        $fieldsStr = implode(', ', array_keys($row));
        $valuesParts = [];
        foreach ($row as $key => $value) {
            $valuesParts[] = '?';
        }
        $valuesStr = implode(', ', $valuesParts);
        $sql = "INSERT INTO {$table} ($fieldsStr) VALUES ($valuesStr)  ";
        $sth = $this->pdo->prepare($sql);
        $sth->execute(array_values($row));
        return $this->pdo->lastInsertId();
    }

    public function delete($table, $where)
    {
        $sql = "DELETE FROM {$table}";
        if (is_array($where) && count($where) > 0) {
            $whereParts = [];
            foreach ($where as $key => $value) {
                $whereParts [] = "{$key}=?";
            }
            $whereStr = implode(' AND ', $whereParts);
            $sql .= ' WHERE ' . $whereStr;

        }
        if (is_string($where)) {
            $sql .= ' WHERE ' . $where;
        }
        $sth = $this->pdo->prepare($sql);
        if (is_array($where) && count($where) > 0) {
            $sth->execute(array_values($where));
        } else {
            $sth->execute();
        }
    }

    public function updata($table, $newRow, $where)
    {
        $sql = "UPDATE {$table} SET ";
        $setParts = [];
        $paramsArr = [];

        foreach ($newRow as $key => $value) {
            $setParts[] = "{$key} = ?";
            $paramsArr[] = $value;
        }
        if (is_array($setParts)) {
            $sql .= implode(', ', $setParts);
        }
        if (is_string($setParts)) {
            $sql .= $setParts;
        }
        if (is_array($where) && count($where) > 0) {
            $whereParts = [];
            foreach ($where as $key => $value) {
                $whereParts [] = "{$key}=?";
                $paramsArr[] = $value;
            }
            $whereStr = implode(' AND ', $whereParts);
            $sql .= ' WHERE ' . $whereStr;
        }
        if (is_string($where)) {
            $sql .= ' WHERE ' . $where;
        }
        $sth = $this->pdo->prepare($sql);
        $sth->execute($paramsArr);
    }
}