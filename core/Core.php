<?php

namespace core;

class Core
{
    private static $instance;
    private static $mainTemplate;
    private  static $DB;
    private function __construct()
    {
        global $Config;
        spl_autoload_register('\core\Core::__autoload');
        self::$DB=new \core\DB($Config['Database']['Server'],
            $Config['Database']['Username'],
            $Config['Database']['Password'],
            $Config['Database']['Database']
        );

    }
    /**
     * Повертає екземпляр ядра системи
     */

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new Core();
            return self::getInstance();
        } else {
            return self::$instance;
        }
    }
    /**
     * Повертає екземпляр об'єкта для зв'язку з базою дани
     */

    public function getDB(){
        return self::$DB;
}
    /**
     * Ініціалізація ядра та створення шаблону
     */
    public function init()
    {
        session_start();
        self::$mainTemplate=new Template();
    }
    /**
     * Автозавантаження класів
     */

    public static function __autoload($className)
    {
        $filename = $className . ".php";
        if (is_file($filename)) {
            include($filename);
        }
    }
    /**
     * Основний процес роботи системи CMS
     */
    public function run()
    {
        $path = $_GET['path'];
        $pathParts = explode('/', $path);
        $className =ucfirst( $pathParts[0]);
      if (empty($className)) {
            $fullclassName = 'controler\\Site';
        } else {
            $fullclassName = 'controler\\' . $className;
            $metodname = ucfirst($pathParts[1]);
        }
        if (empty($metodname)) {
            $fullmetodName = 'actionIndex';
        } else {
            $fullmetodName = 'action' . $metodname;
        }
        $controller = new $fullclassName();
        if (class_exists($fullclassName)) {
            $controller = new $fullclassName();
            if (method_exists($controller, $fullmetodName)) {
                $method =new \ReflectionMethod($fullclassName, $fullmetodName);
                $paramsArray = [];
                foreach ($method->getParameters() as $parametr) {
                  array_push($paramsArray,isset($_GET[$parametr->name])?$_GET[$parametr->name]:null);
                }
               $result= $method->invokeArgs( $controller,$paramsArray);
                if(is_array($result)){
                    self::$mainTemplate->setParams($result);
                }
            } else {
                header('Location: /');
                throw new \Exception('404 Not Found');
            }
        } else {
            throw new \Exception('404 Not Found');
        }


    }
    /**
     * Завершення роботи та підготовка шаблону
     */

    public function  done(){
self::$mainTemplate->display('views/layout/index.php');
    }
}