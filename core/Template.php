<?php
/**
 * Шаблонізатор
*/
namespace core;

class Template
{
    protected $parametrs;
    public function __construct()
    {
      $this->parametrs = [];
    }

    public function setParam($name, $value)
    {
       $this->parametrs[$name]=$value;
    }
    public function getParam($name){
      return $this-> parametrs[$name];
    }
    public function setParams($array)
    {
        foreach ($array as $key=>$value){
            $this->parametrs[$key]=$value;
        }
    }
    public function render($path){
        extract($this->parametrs);
        ob_start();
      include ($path);
      $html=ob_get_contents();
      ob_end_clean();
      return $html;
    }
    public function  display($path){
          echo $this->render($path);
    }
}