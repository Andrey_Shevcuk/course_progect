<?php

namespace controler;
/**
 * контролер заказів
 */
use core\Controler;

class Zakaz extends Controler
{
    protected $user;
    protected $userModel;
    protected $zakazModel;

    public function __construct()
    {
        $this->userModel = new \models\Users();
        $this->zakazModel = new \models\Zakaz();
        $this->user = $this->userModel->getCurrentUser();
    }

    public function actionIndex()
    {
        $auth = $this->userModel->isUserAuth();
        $title = 'Мої замовлення';
        if ($auth === true) {
            $row = $this->zakazModel->getZakaz();
            return $this->render('index', ['model' => $row],
                ['MainTitle' => $title,
                    'PageTitle' => $title]);
        } else {
            return $this->render('index', null,
                ['MainTitle' => $title,
                    'PageTitle' => $title]);
        }
    }

    public function actionAdd()
    {
        $row = $this->zakazModel->AddProduct();
        for ($i = 0; $i < count($row); $i++) {
            setcookie("$row[$i]", "", time() - 300, "/");
        }
        setcookie("sum", "", time() - 300, "/");
        $title = 'Мої замовлення';
        header('Location: /zakaz/index');
        return $this->render('index', null,
            ['MainTitle' => $title,
                'PageTitle' => $title]);

    }
}