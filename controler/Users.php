<?php

namespace controler;

/**
 * контролер користувачів
 */
use core\Controler;

class Users extends Controler
{
    protected $usersModel;

    function __construct()
    {
        $this->usersModel = new \models\Users();
        $this->productModel = new \models\Product();
    }
function  actionLogout(){
    $title='Вихід';

     $row=   $this->productModel->GetKey($_COOKIE);
    for ($i = 0; $i < count($row); $i++) {
        setcookie("$row[$i]", "", time() - 300, "/");
    }
    setcookie("sum", "", time() - 300, "/");
       unset($_SESSION['user']);
  return $this->rendermassage('ok', 'Ви вийши з вашого акаунту', null, [
      'PageTitle' => $title,
       'MainTitle' => $title
    ]);
}
    function actionLogin()
    {
        $title = 'Вхід на сайт';
        if (isset($_SESSION['user'])) {
            return $this->rendermassage('ok', 'Ви вже увійшли на сайт', null, [
                'PageTitle' => $title,
                'MainTitle' => $title
            ]);
        }
        if ($this->isPost()) {
            $user = $this->usersModel->AuthUsers($_POST['login'], $_POST['password']);
            if (!empty($user)) {
                $_SESSION['user'] = $user;
                return $this->rendermassage('ok', 'Ви успішно ввійшли на сайт', null, [
                    'PageTitle' => $title,
                    'MainTitle' => $title
                ]);
            } else {
                return $this->render('login', 'Користувач успішно зареєстрований', [
                    'PageTitle' => $title,
                    'MainTitle' => $title,
                    'MassageText' => 'Неправиьний логін або пароль',
                    'MassageClass' => 'danger'
                ]);
            }
        } else {
            $params = [
                'PageTitle' => $title,
                'MainTitle' => $title
            ];
            return $this->render('login', null, $params);
        }

    }

    function actionRegister()
    {
        if ($this->isPost()) {
            $result = $this->usersModel->AddUsers($_POST);
            if ($result === true) {
                return $this->rendermassage('ok', 'Користувач успішно зареєстрований', null, [
                    'PageTitle' => 'Реєстрація на сайті',
                    'MainTitle' => 'Реєстрація на сайті'
                ]);
            } else {
                $massage = implode('<br/>', $result);
                return $this->render('register', 'Користувач успішно зареєстрований', [
                    'PageTitle' => 'Реєстрація на сайті',
                    'MainTitle' => 'Реєстрація на сайті',
                    'MassageText' => $massage,
                    'MassageClass' => 'danger'
                ]);
            }
        } else {
            $params = [
                'PageTitle' => 'Реєстрація на сайті',
                'MainTitle' => 'Реєстрація на сайті'
            ];
            return $this->render('register', null, $params);
        }
    }
}