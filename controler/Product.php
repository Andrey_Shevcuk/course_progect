<?php

namespace controler;
/**
 * контролер продуктів
 */
use core\Controler;

class Product extends Controler
{
    protected $user;
    protected $userModel;
    protected $productModel;

    public function __construct()
    {
        $this->userModel = new \models\Users();
        $this->productModel = new \models\Product();
        $this->user = $this->userModel->getCurrentUser();
    }

    public function actionIndex()
    {
        global $Config;
        $title = 'Товари';
        $count = $this->productModel->GetCountProduct($Config['ProductCount']);
        return $this->render('index',
            ['count' => $count],
            ['MainTitle' => $title,
                'PageTitle' => $title]);
    }

//Перегляд повної інформації про продукт
    public function actionView()
    {
        $id = $_GET['id'];
        $product = $this->productModel->GetProductById($id);
        $title = $product['name'];
        return $this->render('view', ['model' => $product], [
            'PageTitle' => $title,
            'MainTitle' => $title
        ]);

    }

    public function actionAdd()
    {
        $titlef = 'Доступ заборонено';
        if (empty($this->user)) {
            return $this->render('forbiden', null, [
                'PageTitle' => $titlef,
                'MainTitle' => $titlef
            ]);
        }
        $title = 'Додавання товар';
        if ($this->isPost()) {

            $result = $this->productModel->AddProduct($_POST);
            if ($result['error'] === false) {

                $types = ['image/png', 'image/jpeg'];
                if (is_file($_FILES['file']['tmp_name']) && in_array($_FILES['file']['type'], $types)) {
                    switch ($_FILES['file']['type']) {
                        case'image/png':
                            $exept = 'png';
                            break;
                        default:
                            $exept = 'jpg';
                    }
                    $name = $result['id'] . '_' . uniqid() . '.' . $exept;
                    move_uploaded_file($_FILES['file']['tmp_name'], 'files/product/' . $name);
                    $this->productModel->ChagePhoto($result['id'], $name);
                }
                return $this->rendermassage('ok', 'Товар усппішно додано', null, [
                    'PageTitle' => $title,
                    'MainTitle' => $title
                ]);
            } else {
                $massage = implode('<br/>', $result['massages']);
                return $this->render('form', ['model' => $_POST], [
                    'PageTitle' => $title,
                    'MainTitle' => $title,
                    'MassageText' => $massage,
                    'MassageClass' => 'danger'
                ]);
            }
        }
        return $this->render('form', ['model' => $_POST], [
            'PageTitle' => $title,
            'MainTitle' => $title
        ]);

    }

    public function actionEdit()
    {

        $id = $_GET['id'];
        $user = $this->userModel->getCurrentUser();
        $product = $this->productModel->GetProductById($id);
        $titlef = 'Доступ заборонено';
            if (empty($this->user) || $product['user_id'] != $this->userModel->getCurrentUser()['id']) {
                if($user['acses'] == 1){

                }
                else {
                    return $this->render('forbiden', null, [
                        'PageTitle' => $titlef,
                        'MainTitle' => $titlef
                    ]);
                }
            }

            $title = 'Редагування товару';
            if ($this->isPost()) {
                $result = $this->productModel->UpdateProduct($_POST, $id);
                if ($result === true) {
                    $types = ['image/png', 'image/jpeg'];
                    if (is_file($_FILES['file']['tmp_name']) && in_array($_FILES['file']['type'], $types)) {
                        switch ($_FILES['file']['type']) {
                            case'image/png':
                                $exept = 'png';
                                break;
                            default:
                                $exept = 'jpg';
                        }
                        $name = $id . '_' . uniqid() . '.' . $exept;
                        move_uploaded_file($_FILES['file']['tmp_name'], 'files/product/' . $name);
                        $this->productModel->ChagePhoto($id, $name);
                    }
                    return $this->rendermassage('ok', 'Товар усппішно відредаговано', null, [
                        'PageTitle' => $title,
                        'MainTitle' => $title
                    ]);
                } else {
                    $massage = implode('<br/>', $result);
                    return $this->render('form', ['model' => $product], [
                        'PageTitle' => $title,
                        'MainTitle' => $title,
                        'MassageText' => $massage,
                        'MassageClass' => 'danger'
                    ]);
                }
            }
            return $this->render('form', ['model' => $product], [
                'PageTitle' => $title,
                'MainTitle' => $title
            ]);


    }

    public function actionDelete()
    {
        $title = 'Видалення товару';
        $id = $_GET['id'];
        $user = $this->userModel->getCurrentUser();
        $product=$this->productModel->GetProductById($id);
        if (isset($_GET['confirm']) && $_GET['confirm'] == 'yes') {

            if($user['acses']==1){
                $this->productModel->DeleteProduct($id);
          header('Location: /product/');
            }
         elseif ($user['id']==$product['user_id'])//
                 {
                     $this->productModel->DeleteProduct($id);
                header('Location: /product/');
            }
           else {
             return $this->rendermassage('error', 'Помилка видалення новини', null, [
                  'PageTitle' => $title,
                   'MainTitle' => $title
               ]);
            }
        }
        $product = $this->productModel->GetProductById($id);
        return $this->render('delete', ['model' => $product], [
           'PageTitle' => $title,
            'MainTitle' => $title
        ]);
    }
}
