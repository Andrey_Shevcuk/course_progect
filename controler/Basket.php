<?php

namespace controler;
/**
 * контролер корзини
*/
use core\Controler;

class Basket extends Controler
{

    protected $user;
    protected $userModel;
    protected $basketModel;

    public function __construct()
    {
        $this->userModel = new \models\Users();
        $this->basketModel = new \models\Basket();
        $this->user = $this->userModel->getCurrentUser();
    }
    public function actionIndex()
    {
        //  global $Config;
        $title = 'Корзина';
        return $this->render('index', null,
            ['MainTitle' => $title,
                'PageTitle' => $title]);
    }
}